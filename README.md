# Springboot-dubbo-mybatisplus

#### 项目介绍
{
    个人搭建的demo仅供参考,希望有大神能指点一二
}

#### 软件架构
SpringBoot Dubbo Mybatis


#### 安装教程

本人使用的是myeclispe2016,当然其实idea也可以用

#### 使用说明
进入demoApplication运行即可,但是需要搭建service端,可访问我另一个service端的项目地址
https://gitee.com/itCjb/Springboot-dubbo-mybatisplus-Service
附上一个dubbo-admin的管理搭建教程
https://www.cnblogs.com/shengulong/p/8303454.html

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)