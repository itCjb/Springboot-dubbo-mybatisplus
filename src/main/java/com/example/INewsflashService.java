package com.example;

import com.baomidou.mybatisplus.service.IService;
import com.baomidou.springboot.entity.Newsflash;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author FUNKYE
 * @since 2018-09-03
 */
public interface INewsflashService extends IService<Newsflash> {

}
