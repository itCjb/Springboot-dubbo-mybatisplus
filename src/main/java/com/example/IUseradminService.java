package com.example;

import org.springframework.stereotype.Component;

import com.baomidou.mybatisplus.service.IService;
import com.baomidou.springboot.entity.Useradmin;
/**
 * <p>
 *  服务类
 * </p>
 *
 * @author FUNKYE
 * @since 2018-09-03
 */
public interface IUseradminService extends IService<Useradmin> {

}
