package com.example;

public interface RedisService {
	public void set(String key, String value);
	public void set(String key, Object value);
	public void setWithExpireTime(String key, String value, int exptime) ;
	public String get(String key);
	public Object getObj(String key);
	public void del(String key);
	public void expire(String key, Object value, Integer times);
}
