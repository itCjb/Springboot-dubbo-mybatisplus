package com.baomidou.springboot.Interceptor;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Hex;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.dubbo.config.annotation.Reference;
import com.baomidou.springboot.entity.Useradmin;
import com.example.RedisService;

public class adminInterceptor implements HandlerInterceptor {
	@Reference(version = "1.0.0")
	private RedisService rs;

	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean preHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2) throws Exception {
		// TODO Auto-generated method stub

		if (arg0.getParameter("token") != null && arg0.getParameter("token") != "") {
			HttpSession session = arg0.getSession();
			Useradmin useradmin = (Useradmin) rs.getObj(getSHA256Str(arg0.getParameter("token")));
			String url = arg0.getRequestURI();
			if (useradmin != null) {
				System.out.println(useradmin.getPower());
			}
			if (useradmin == null || useradmin.getPower() < 1) {
				arg1.getWriter().printf("{\"msg\":\"不是管理员\",\"obj\":\"\",\"status\":\"404\",\"success\":true}");
				arg1.setHeader("Access-Control-Allow-Origin", "*");
				arg1.setHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS,DELETE");
				arg1.setHeader("Access-Control-Max-Age", "3600");
				arg1.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
				arg1.setContentType("text/html");
				arg1.setCharacterEncoding("utf-8");
				arg1.getWriter().flush();
				arg1.getWriter().close();
				System.out.println("不是管理员");
				return false;
			} else {
				rs.expire(getSHA256Str(arg0.getParameter("token")), useradmin, 30 * 60);
				// System.out.println(arg0.getParameter("token"));
				return true;
			}
		} else {
			arg1.getWriter().printf("{\"msg\":\"不是管理员\",\"obj\":\"\",\"status\":\"200\",\"success\":true}");
			arg1.setHeader("Access-Control-Allow-Origin", "*");
			arg1.setHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS,DELETE");
			arg1.setHeader("Access-Control-Max-Age", "3600");
			arg1.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
			arg1.setContentType("text/html");
			arg1.setCharacterEncoding("utf-8");
			arg1.getWriter().flush();
			arg1.getWriter().close();
			System.out.println("不是管理员");
			return false;
		}
	}

	public String getSHA256Str(String str) {
		MessageDigest messageDigest;
		String encdeStr = null;
		try {
			messageDigest = MessageDigest.getInstance("SHA-256");
			byte[] hash = messageDigest.digest(str.getBytes("UTF-8"));
			encdeStr = Hex.encodeHexString(hash);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		str = null;
		return encdeStr;
	}
}
