package com.baomidou.springboot.bean;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Map;

public class Bean implements Serializable {
	private String[] select;
	private String odb;
	private String limit;
	Map<String, Object> map;
	public String[] getSelect() {
		return select;
	}
	public void setSelect(String... select) {
		this.select = select;
	}
	public String getOdb() {
		return odb;
	}
	public void setOdb(String odb) {
		this.odb = odb;
	}
	public String getLimit() {
		return limit;
	}
	public void setLimit(String limit) {
		this.limit = limit;
	}
	public Map<String, Object> getMap() {
		return map;
	}
	public void setMap(Map<String, Object> map) {
		this.map = map;
	}
	@Override
	public String toString() {
		return "Bean [select=" + Arrays.toString(select) + ", odb=" + odb + ", limit=" + limit + ", map=" + map + "]";
	}

	
}
