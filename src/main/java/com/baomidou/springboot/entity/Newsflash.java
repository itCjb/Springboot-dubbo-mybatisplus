package com.baomidou.springboot.entity;


import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author FUNKYE
 * @since 2018-09-03
 */
public class Newsflash implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String title;
    private Date date;
    private String content;
    @TableField("useradminId")
    private Integer useradminId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getUseradminId() {
        return useradminId;
    }

    public void setUseradminId(Integer useradminId) {
        this.useradminId = useradminId;
    }

    @Override
    public String toString() {
        return "Newsflash{" +
        ", id=" + id +
        ", title=" + title +
        ", date=" + date +
        ", content=" + content +
        ", useradminId=" + useradminId +
        "}";
    }
}
