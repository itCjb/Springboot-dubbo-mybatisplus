package com.baomidou.springboot.controller;

import org.springframework.web.bind.annotation.ModelAttribute; 
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.dubbo.config.annotation.Reference;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.springboot.bean.Token;
import com.baomidou.springboot.entity.TokenModel;
import com.baomidou.springboot.entity.Useradmin;
import com.example.IUseradminService;
import com.example.RedisService;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author FUNKYE
 * @since 2018-09-03
 */
@Controller
public class UseradminController extends BaseController {
	@Reference(version = "1.0.0")
	private RedisService rs;
	@Reference(version = "1.0.0")
	private IUseradminService us;



	@RequestMapping(value = "login", method = RequestMethod.GET)
	@ResponseBody
	public Object login(@ModelAttribute Useradmin useradmin, HttpSession session) throws Exception {
		if (useradmin != null && useradmin.getName() != null && useradmin.getPassword() != null) {
			Assert.notNull(useradmin.getName(), "username can not be empty");
			Assert.notNull(useradmin.getPassword(), "password can not be empty");
			Useradmin Useradmin = null;
			useradmin.setPassword(getSHA256Str(encrypByMd5Jar(useradmin.getPassword())));
			EntityWrapper<Useradmin> ew = new EntityWrapper<Useradmin>();
			ew.setEntity(new Useradmin());
			ew.eq("password", useradmin.getPassword()).eq("name", useradmin.getName());
			List<Useradmin> u = us.selectList(ew);
			System.out.println(useradmin.getName());
			if (u != null && u.size() > 0) {
				Useradmin = u.get(0);
				if (Useradmin.getPower() > 0) {
					// session.setAttribute("useradmin", Useradmin);
					/*
					 * Jedis jedis = new Jedis("172.17.0.1",6379); String
					 * admin=getSHA256Str("useradmin" + useradmin.getId());
					 * byte[] b=admin.getBytes(); jedis.set(b,
					 * SerializeUtil.serialize(Useradmin)); jedis.expire(b,
					 * 60*30);
					 */
					Token token=new Token();
					token.setObj(Useradmin);
					token.setToken(encrypByMd5Jar(session.getId()));
					rs.expire(getSHA256Str(encrypByMd5Jar(session.getId())), Useradmin,60*30);
					return renderSuccess(token);
				} else {
					return renderError("帐号已被停用");
				}
			} else {
				return renderError("用户名或密码错误");
			}
		} else {
			return renderError("用户名或密码错误");
		}
	}

	public String encrypByMd5Jar(String context) {
		return DigestUtils.md5Hex(context);

	}

	/***
	 * 利用Apache的工具类实现SHA-256加密
	 * 
	 * @param str
	 *            加密后的报文
	 * @return
	 */
	public String getSHA256Str(String str) {
		MessageDigest messageDigest;
		String encdeStr = null;
		try {
			messageDigest = MessageDigest.getInstance("SHA-256");
			byte[] hash = messageDigest.digest(str.getBytes("UTF-8"));
			encdeStr = Hex.encodeHexString(hash);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		str = null;
		return encdeStr;
	}
}
