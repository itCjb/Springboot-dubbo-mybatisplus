package com.baomidou.springboot.controller;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.dubbo.config.annotation.Reference;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.springboot.entity.Newsflash;
import com.baomidou.springboot.entity.Useradmin;
import com.example.INewsflashService;
import com.example.IUseradminService;
import com.example.RedisService;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Hex;
import org.springframework.stereotype.Controller;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author FUNKYE
 * @since 2018-09-03
 */
@Controller
public class NewsflashController extends BaseController {
	@Reference(version = "1.0.0")
	private INewsflashService ns;
	@Reference(version = "1.0.0")
	private RedisService rs;

	@RequestMapping(value = "admin/getNewsflashall", method = RequestMethod.GET)
	@ResponseBody
	public Object all(Integer start, Integer size) {
		if (size != null && start != null) {
			EntityWrapper<Newsflash> ew = new EntityWrapper<Newsflash>();
			ew.setEntity(new Newsflash());
			ew.setSqlSelect("id", "title", "date");
			Page<Newsflash> page = new Page<Newsflash>(start, size, "id", false);
			return renderSuccess(ns.selectPage(page, ew));
		} else {
			return renderError("参数错误");
		}
	}

	@RequestMapping(value = "admin/addNewsflash", method = RequestMethod.GET)
	@ResponseBody
	public Object addNewsflash(@ModelAttribute Newsflash obj, HttpServletRequest req, Useradmin useradmin)
			throws Exception {
		if (req.getParameter("token") != null) {
			useradmin = (Useradmin) rs.getObj(getSHA256Str(req.getParameter("token")));
		} else {
			return renderError("请检查信息是否为空");
		}
		if (useradmin == null) {
			return renderError("请检查信息是否为空");
		}
		if (obj != null && obj.getTitle() != null && obj.getTitle().length() > 0) {
			// System.out.println(Useradmins.toString());
			EntityWrapper<Newsflash> ew = new EntityWrapper<Newsflash>();
			ew.setEntity(new Newsflash());
			ew.eq("title", obj.getTitle());
			List<Newsflash> u = ns.selectList(ew);
			if (u != null && u.size() > 0) {
				return renderError("标题已存在");
			} else {

				if (useradmin != null) {
					obj.setUseradminId(useradmin.getId());
				}
				obj.setDate(new Date());
				ns.insert(obj);
				return renderSuccess("成功");
			}
		} else {
			return renderError("请检查信息是否为空");
		}

	}

	@RequestMapping(value = "admin/delNewsflash", method = RequestMethod.GET)
	@ResponseBody
	public Object delNewsflash(Integer id, HttpSession session) throws Exception {
		if (id != null) {
			Newsflash obj = ns.selectById(id);
			if (obj != null) {
				ns.deleteById(id);
				return renderSuccess("成功");
			} else {
				return renderError("不存在");
			}
		} else {
			return renderError("不存在");
		}
	}

	public String getSHA256Str(String str) {
		MessageDigest messageDigest;
		String encdeStr = null;
		try {
			messageDigest = MessageDigest.getInstance("SHA-256");
			byte[] hash = messageDigest.digest(str.getBytes("UTF-8"));
			encdeStr = Hex.encodeHexString(hash);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		str = null;
		return encdeStr;
	}
}
